var model = {
    user: "Linda",
    items: [{action: "Buy Flowers", done: false},
            {action: "Get shoes", done: false},
            {action: "collect Tickets", done: true},
            {action: "Call Joe", done: false}]
}

var todoApp =  angular.module('todoApp', []);

todoApp.controller("ToDoCtrl", function($scope){
    $scope.todo = model;
});